### Preparation
Put https://gitlab.com/famedly/ansible/collections/matrix under `ansible_collections/famedly/matrix`.
Run `ansible-galaxy collection install community.postgresql`.

### Running
Run with `ansible-playbook --ask-vault-pass -i inventories/matrix/inventory.yml playbooks/<playbook>` first the postgres one, then the matrix one.

New vault variables can be created with: `ansible-vault encrypt_string --stdin-name 'matrix_synapse_pg_pass'`.

### Todo

* SSO signin only (but this means bots need to get an access token first)
* add chat.codeberg-test.org to SSO client whitelist
* Email notification support
* Fully check through synapse config file once (i.e. url preview url blacklist is currently set but shouldn't I think)
* Set up metrics (synapse -> prometheus, prometheus-node-exporter + grafana)
* numeric uids from codeberg are currently not working
* set up [Mjolnir](https://github.com/matrix-org/mjolnir) for moderation
* Limit room complexity that synapse is allowed to join
* allow adding to room directory only via codeberg interface?
* fully allow managing rooms+permissions through codeberg
* enable using workers through synapse role
* probably many more things...

### Haproxy config snippets

Needs haproxy >=2.2 (i.e. buster-backports)

```
	http-request return content-type "application/json" string "{\"m.server\": \"matrix.codeberg-test.org:443\"}" if { path /.well-known/matrix/server }
	http-request return content-type "application/json" string "{\"m.homeserver\": {\"base_url\": \"https://matrix.codeberg-test.org\"}}" hdr Access-Control-Allow-Origin "*" if { path /.well-known/matrix/client }
	acl host_matrix hdr(host) -i matrix.codeberg-test.org:443
	acl host_matrix hdr(host) -i matrix.codeberg-test.org
	acl host_chat hdr(host) -i chat.codeberg-test.org
	acl matrix-path path_beg /_matrix
	acl matrix-path path_beg /_synapse/client
	use_backend matrix if host_matrix matrix-path
	use_backend element if host_chat

backend matrix
	server matrix 192.168.2.4:8008

backend element
	server matrix 192.168.2.4:8080
```
